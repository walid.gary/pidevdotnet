namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class zeall : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Quizs", "DateQuizEnd");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Quizs", "DateQuizEnd", c => c.DateTime(precision: 0));
        }
    }
}
