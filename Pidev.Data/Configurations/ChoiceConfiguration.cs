﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pidev.Domain.Entities;

namespace Pidev.Data.Configurations
{
    public class ChoiceConfiguration : EntityTypeConfiguration<Choice>
    {
        public ChoiceConfiguration()
        {
            HasOptional(p => p.Question)   
                .WithMany(c => c.Choices)
                .HasForeignKey(p => p.QuestionID)
                .WillCascadeOnDelete(true);
        }
    }
}
