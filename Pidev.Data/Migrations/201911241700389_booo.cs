namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class booo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "FormationID", c => c.Int());
            CreateIndex("dbo.Users", "FormationID");
            AddForeignKey("dbo.Users", "FormationID", "dbo.formation", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "FormationID", "dbo.formation");
            DropIndex("dbo.Users", new[] { "FormationID" });
            DropColumn("dbo.Users", "FormationID");
        }
    }
}
