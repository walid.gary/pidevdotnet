﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Domain.Entities
{
    public class Video
    {
        public int VideoId { get; set; }
        [DisplayName("Nom de la vidéo")]
        public string NomV { get; set; }
        [DisplayName("Choisir une vidéo")]
        public string PathV { get; set; }
        [DisplayName("Formation")]
        public int? FormationID { get; set; }
        [ForeignKey("FormationID")]
        public formation formation { get; set; }
    }
}
