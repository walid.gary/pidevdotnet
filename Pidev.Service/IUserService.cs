﻿using Pidev.Data;
using Pidev.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Service
{
    public interface IUserService : IService<user>
    {
         IEnumerable<user> GetByEmail(string email);
        user GetUserByEmail(string email);
    }
}
