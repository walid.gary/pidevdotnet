namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addddd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.formation", "adresse", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.formation", "adresse");
        }
    }
}
