﻿using Pidev.Domain.Entities;
using Service.Pattern;
using Solution.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;

namespace Pidev.Service
{
    public class AnswerService : Service<Answer>, IAnswerService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);
        public AnswerService() : base (utk)
        {

        }

        public Answer GetAnswerByQuestion(int? q)
        {
            return Get(f=>f.QuestionID==q); 
        }
       

        
    }
}
