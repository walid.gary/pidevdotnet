﻿using Pidev.Domain.Entities;
using Service.Pattern;

namespace Pidev.Service
{
    public interface IQuizService : IService<Quiz>
    {
        Quiz GetQuizByText(string txt);
    }
}