namespace Pidev.Domain.Entities

{
    using System;
    using System.Collections.Generic;

    public class Quiz
    {
        public Quiz()
        {
            this.Questions = new HashSet<Question>();
        }

        public int QuizID { get; set; }
        public string QuizName { get; set; }
        public ICollection<Resultat> Resultats { get; set; }
        public DateTime? DateQuizStart { get; set; }
        public string Color { get; set; }
       
        public virtual ICollection<Question> Questions { get; set; }
    }
}
