﻿
using Pidev.Domain.Entities;
using Service.Pattern;
using Solution.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Service
{
    public class QuestionService : Service<Question>, IQuestionService
    {
        
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);
        public QuestionService() : base(utk)
        {

        }
        public IEnumerable<Question> GetQuestionByQuiz(int QuizID)
        {
            return GetMany(f => f.QuizID == QuizID);
        }
       
        public IQueryable<Question> GetQuestionByQuiz2(int QuizID)
        {
            IQueryable<Question> questions;
            questions = GetMany(f => f.QuizID == QuizID).Select(q => new Question

            {
                QuestionID = q.QuestionID,
                QuestionText = q.QuestionText,
                Choices = q.Choices.Select(c => new Choice
                {
                    ChoiceID = c.ChoiceID,
                    ChoiceText = c.ChoiceText
                }).ToList()
                }).AsQueryable();
            
            return (questions);
        }
        public Question GetQuesByText(string txt)
        {
            return Get(f => f.QuestionText.Equals(txt));
        }

       

    }
}
