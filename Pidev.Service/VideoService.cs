﻿using Pidev.Domain.Entities;
using Service.Pattern;
using Solution.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Service
{
    public class VideoService : Service<Video>, IVideoService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);
        public VideoService() : base (utk)
        {

        }
    }
}
