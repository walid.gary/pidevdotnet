﻿using Pidev.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pidev.Presentation.Models
{
    public class UserVM
    {
        public int UserID { get; set; }
        [Required]
        public string FullName { get; set; }
        public string ProfilImage { get; set; }
        public string Email { get; set; }
        public ICollection<Resultat> Resultats { get; set; }
    }
    public class QuizVM2
    {
        public int QuizID { get; set; }
        public string QuizName { get; set; }
        public DateTime? DateQuizStart { get; set; }
        public string Color { get; set; }
    }

        public class QuizVM
    {
        public int QuizID { get; set; }
        public string QuizName { get; set; }
        public DateTime? DateQuizStart { get; set; }
        public DateTime? DateQuizEnd { get; set; }

        
        public List<SelectListItem> ListOfQuizz { get; set; }
        public ICollection<Question> Questions { get; set; }
        public ICollection<Answer> Answers { get; set; }
        //public Choice Choice { get; set; }
        
    }

    public class QuestionVM
    {
        public int QuestionID { get; set; }
        public string QuestionText { get; set; }
        //public string QuestionType { get; set; }
        public string choix1 { get; set; }
        public string choix2 { get; set; }
        public string choix3 { get; set; }
        public string choix4 { get; set; }
        // public string Anwser { get; set; }
        public  ICollection<ChoiceVM> Choices { get; set; }
        [Display(Name = "Quiz")]
        public int? QuizID { get; set; }
        public IEnumerable<SelectListItem> Quizs { get; set; }
        public List<SelectListItem> ListOfQuizz { get; set; }
    }

    public class ChoiceVM
    {
        public int ChoiceID { get; set; }
        public string ChoiceText { get; set; }
    }

    public class QuizAnswersVM
    {
        public int? QuestionID { get; set; }
        public string QuestionText { get; set; }
        public string AnswerQ { get; set; }
        public bool isCorrect { get; set; }

    }

    public class DoItInOneStep
    {
        [Required]
        public string QuizName { get; set; }
        [Required]
        public string QuestionText { get; set; }
        [Required]
        public string choix1 { get; set; }
        [Required]
        public string choix2 { get; set; }
        [Required]
        public string choix3 { get; set; }
        [Required]
        public string choix4 { get; set; }
        [Required]
        public string AnswerQ { get; set; }
    }
   public class PieChart
    {
        public int NbUsersI { get; set; }
        public int NbUsersJ { get; set; }
        public int NbUsersJ2 { get; set; }
        public int NbUsersD { get; set; }
        public int NbUsersA { get; set; }
    }
    public class ResultatVM
    {
        public int ResultatID { get; set; }
        public string Note { get; set; }
        public string FullName { get; set; }
        public string NomQuiz { get; set; }
        public int? UsersID { get; set; }
        public Users Users { get; set; }
        public int? QuizID { get; set; }
        public Quiz Quiz { get; set; }
        public DateTime DatePassageExam { get; set; }
        public float NoteFloat { get; set; }
        public bool isChecked { get; set; }
 
    }
    public class ResultatListVM
    {
        public List<ResultatVM> liste { get; set; }
    }
}