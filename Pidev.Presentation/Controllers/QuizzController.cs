﻿using OfficeOpenXml;
using Org.BouncyCastle.X509;
using Pidev.Domain.Entities;
using Pidev.Presentation.Models;
using Pidev.Service;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Pidev.Presentation.Controllers
{
    public class QuizzController : Controller
    {
        IVideoService vss;
        IformationService formationService;
        IAnswerService answerService;
        IChoiceService choiceService;
        IQuestionService questionService;
        IQuizService quizService;
        IUsersService userService;
        IResultatService resultats;

        public QuizzController()
        {
            vss = new VideoService();
            formationService = new formationService();
            answerService = new AnswerService();
            choiceService = new ChoiceService();
            questionService = new QuestionService();
            quizService = new QuizService();
            userService = new UsersService();
            resultats = new ResultatService();
        }

        public ActionResult LogOff()
        {
            Session.Contents.RemoveAll();
            return RedirectToAction("GetUser");
        }
        
        public ActionResult Index()
        {
            UserVM userConnected = Session["UserConnected"] as UserVM;
            if (userConnected == null)
            {
                return RedirectToAction("GetUser");
            }
            return View();
        }

        [HttpGet]
        public ActionResult GetUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetUser(UserVM user)
        {
            if (ModelState.IsValid) { 
            Users user1 = new Users
            {
                FullName = user.FullName,
            };
            Users userCo = userService.GetUsersByFullName(user1);
           if (userCo != null) { 
            UserVM userConnected = new UserVM
            {
                FullName = userCo.FullName,
                ProfilImage = userCo.ProfilImage,
                UserID = userCo.UsersID,
                Email = userCo.Email
            };

                Session["UserConnected"] = userConnected;
                    if (userConnected.FullName.Equals("Gary Gary"))
                    {
                        return RedirectToAction("IndexFormation");
                    }
                    else { 
                return RedirectToAction("SelectQuizz");
                    }
                }
            }
            else
            {
                ViewBag.Msg = "Sorry : user is not found !!";
                return View();
             }
           
            return View();
        }
   
        [HttpGet]
        public ActionResult SelectQuizz()
        {
            UserVM userConnected = Session["UserConnected"] as UserVM;
            if (userConnected == null)
            {
                return RedirectToAction("GetUser");
            }
            List<QuizVM> maliste = new List<QuizVM>();
            IEnumerable<Quiz> quizs = quizService.GetMany();
            foreach (var quizz in quizs)
            {
                maliste.Add(new QuizVM
                {
                    QuizID = quizz.QuizID,
                    QuizName = quizz.QuizName,

                });
            }
            QuizVM quiz = new QuizVM();
            quiz.ListOfQuizz = maliste.Select(q => new SelectListItem
            {
                Text = q.QuizName,
                Value = q.QuizID.ToString()

            }).ToList();

            return View(quiz);
        }

        [HttpPost]
        public ActionResult SelectQuizz(QuizVM quiz)
        {
            UserVM UserConnected = Session["UserConnected"] as UserVM;
            var x = resultats.GetMany().ToList();
            Quiz quiz2 = quizService.Get(q => q.QuizID == quiz.QuizID);
            QuizVM quizSelected = new QuizVM
            {
                QuizName = quiz2.QuizName,
                QuizID = quiz2.QuizID,
                Questions = quiz.Questions
            };
            if (x.Count == 0)
            {
                Session["SelectedQuiz"] = quizSelected;
                return RedirectToAction("QuizTest");
            }
            foreach (var ii in x)
                {
                    if (ii.QuizID == quiz2.QuizID && ii.UsersID == UserConnected.UserID && quizSelected != null)
                    {
                    TempData["MessageI"] = "You have already passed this exam !";

                    return RedirectToAction("SelectQuizz");
                    
                    }
                }
            Session["SelectedQuiz"] = quizSelected;
            return RedirectToAction("QuizTest");

        }

        [HttpGet]
        public ActionResult QuizTest()
        {
            UserVM userConnected = Session["UserConnected"] as UserVM;
            if (userConnected == null)
            {
                return RedirectToAction("GetUser");
            }
            QuizVM quizSelected = Session["SelectedQuiz"] as QuizVM;
            List<QuestionVM> maliste = new List<QuestionVM>();
            IEnumerable<Question> ques = questionService.GetQuestionByQuiz(quizSelected.QuizID);
            if (quizSelected != null)
            {
                Quiz quiz = new Quiz
                {
                    QuizID = quizSelected.QuizID,
                    QuizName = quizSelected.QuizName,
                    Questions = quizSelected.Questions
                };

                ques = questionService.GetQuestionByQuiz(quiz.QuizID);

                foreach (var question in ques)
                {

                    maliste.Add(new QuestionVM
                    {
                        QuestionID = question.QuestionID,
                        QuestionText = question.QuestionText,
                        choix1 = question.choix1,
                        choix2 = question.choix2,
                        choix3 = question.choix3,
                        choix4 = question.choix4
                    });
                }

            }

            return View(maliste);

        }

        [HttpPost]
        public ActionResult QuizTest(List<QuizAnswersVM> resultQuiz)
        {
            foreach (QuizAnswersVM p in resultQuiz)
            {
                if (p.AnswerQ == null)
                {
                    p.AnswerQ = "dsqdsqdsq";
                }
            }
            List<QuizAnswersVM> finalResultQuiz = new List<QuizAnswersVM>();
            List<Answer> ans = new List<Answer>();
            foreach (QuizAnswersVM answser in resultQuiz)
            {
                ans.Add(new Answer
                {
                    QuestionID = answser.QuestionID,
                    AnswerText = answser.AnswerQ,
                    QuestionText = answser.QuestionText
                });
            }
            foreach (Answer a in ans)
            {
                string lol = a.AnswerText;
                Answer resultt = answerService.GetAnswerByQuestion(a.QuestionID);
                QuizAnswersVM bb = new QuizAnswersVM
                {
                    QuestionID = resultt.QuestionID,
                    AnswerQ = resultt.AnswerText,
                    isCorrect = (lol.ToLower().Equals(resultt.AnswerText.ToLower()))
                };
                finalResultQuiz.Add(bb);
            }
            return Json(new { result = finalResultQuiz }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ActionResult GetResult()
        {
            QuizVM quizSelected = Session["SelectedQuiz"] as QuizVM;
            UserVM userConnected = Session["UserConnected"] as UserVM;
            string res = Request["res"];
            string ok = "La note est de : " + res + " %";
            float k = Single.Parse(res);
            Resultat result = new Resultat
            {
                
                Note = ok,
                QuizID = quizSelected.QuizID,
                UsersID = userConnected.UserID,
                DatePassageExam = DateTime.Now,
                FullName = userConnected.FullName,
                NomQuiz=quizSelected.QuizName,
                NoteFloat=k
            };

            Session["SelectedRes"] = result;
            resultats.Add(result);
            resultats.Commit();
            return RedirectToAction("Finished");
        }

        [HttpGet]
        public ActionResult Finished()
        {
            UserVM userConnected = Session["UserConnected"] as UserVM;
            if (userConnected == null)
            {
                return RedirectToAction("GetUser");
            }
            QuizVM quizSelected = Session["SelectedQuiz"] as QuizVM;
            Resultat SelectedRes = Session["SelectedRes"] as Resultat;
            ViewBag.user = null;
            ViewBag.user1 = null;
            ViewBag.user2 = null;
            ViewBag.user3= null;
            ViewBag.user4 = null;
            if (SelectedRes.NoteFloat >= 75.0)
            {
                ViewBag.user = "Felicitation Mr/Mme : " + userConnected.FullName;
                ViewBag.user1 = "Vous avez obtenu un score supérieure à 70 % ";
                ViewBag.user2 = "Votre score est de : " + SelectedRes.NoteFloat ;
                ViewBag.user3  =  "Nom du quiz passé  :" + quizSelected.QuizName;
                ViewBag.user4 = "Vous avez reussi à passer cette formation !";
            }
            else if (SelectedRes.NoteFloat >= 50.0 && SelectedRes.NoteFloat < 75.0) {
                ViewBag.user = "Dommage Mr/Mme : " + userConnected.FullName;
                ViewBag.user1 = "Vous avez obtenu un score entre 50 % et 75 % ";
                ViewBag.user2 = "Votre score est de : " + SelectedRes.NoteFloat + " %";
                ViewBag.user3 = "Nom du quiz passé  :" + quizSelected.QuizName;
                ViewBag.user4 = "Vous serez réaffectés par un manager à cette formation !";

            }
            else
            {
                ViewBag.user = "Dommage Mr/Mme : " + userConnected.FullName;
                ViewBag.user1 = "Vous avez obtenu un score inférieure à 50 % ";
                ViewBag.user2 = "Votre score est de : " + SelectedRes.NoteFloat+" %";
                ViewBag.user3 = "Nom du quiz passé  :" + quizSelected.QuizName;
                ViewBag.user4 = "Vous devez contacter l'administration pour régler votre cas !";
            }
            return View();
        }

        [HttpGet]
        public ActionResult ResultUsers()
        {
            UserVM userConnected = Session["UserConnected"] as UserVM;
            if (userConnected == null)
            {
                return RedirectToAction("GetUser");
            }
            List<ResultatVM> maliste = new List<ResultatVM>();
            List<Resultat> ls = resultats.GetMany().OrderByDescending(s => s.NoteFloat).ToList();
            foreach (Resultat r in ls)
            {
                maliste.Add(new ResultatVM
                {
                    ResultatID = r.ResultatID,
                    Note = r.Note,
                    FullName = r.FullName,
                    NomQuiz = r.NomQuiz,
                    UsersID = r.UsersID,
                    QuizID = r.QuizID,
                    DatePassageExam = r.DatePassageExam,
                    NoteFloat = r.NoteFloat,
                    isChecked = r.isChecked
                });
            }
            ResultatListVM res = new ResultatListVM();
            res.liste = maliste;
            return View(res);
        }

        [HttpPost]
        public ActionResult ResultUsers(ResultatListVM rs)
        {
            List<ResultatVM> maliste = new List<ResultatVM>();
            List<Users> malistee = new List<Users>();
            foreach (var item in rs.liste)
            {
                if (item.isChecked)
                {
                    maliste.Add(item);
                }
            }
            foreach (var item in maliste)
            {
                Users u = userService.GetUsersByFullNameee(item.FullName);
                malistee.Add(u);
            }
            Session["SelectedUsers"] = malistee;

            return RedirectToAction("IndexFormation");

        }

        public ActionResult GetData()
        {

            int NbUsersI = resultats.GetMany(x => x.NomQuiz.Equals("informatique")).Count();
            int NbUsersJ2 = resultats.GetMany(x => x.NomQuiz.Equals("JEE")).Count();
            int NbUsersJ = resultats.GetMany(x => x.NomQuiz.Equals("JAVA")).Count();
            int NbUsersD = resultats.GetMany(x => x.NomQuiz.Equals("DotNet")).Count();
            int NbUsersA = resultats.GetMany(x => x.NomQuiz.Equals("DotNet")).Count();
            PieChart pie = new PieChart();
            pie.NbUsersJ2 = NbUsersJ2;
            pie.NbUsersJ = NbUsersJ;
            pie.NbUsersI = NbUsersI;
            pie.NbUsersD = NbUsersD;
            pie.NbUsersA = NbUsersA;
            return Json(pie, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Chat()
        {
            UserVM userConnected = Session["UserConnected"] as UserVM;
            if (userConnected == null)
            {
                return RedirectToAction("GetUser");
            }
            return View();
        }

        [HttpGet]
        public ActionResult IndexFormation()
        {
            UserVM userConnected = Session["UserConnected"] as UserVM;
            if (userConnected == null)
            {
                return RedirectToAction("GetUser");
            }

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:9080");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("pidev-web/api/projects").Result;
            if (response.IsSuccessStatusCode)
            {
                ViewBag.result = response.Content.ReadAsAsync<IEnumerable<formation>>().Result;
                ViewBag.users = userService.GetMany();
            }
            else
            {
                ViewBag.result = "error";
            }
            return View();
        }

        [HttpGet]
        public ActionResult IndexFormationUser()
        {
            UserVM userConnected = Session["UserConnected"] as UserVM;
            if (userConnected == null)
            {
                return RedirectToAction("GetUser");
            }
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:9080");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("pidev-web/api/projects").Result;
            if (response.IsSuccessStatusCode)
            {
                ViewBag.result = response.Content.ReadAsAsync<IEnumerable<formation>>().Result;
                ViewBag.users = userService.GetMany();
            }
            else
            {
                ViewBag.result = "error";
            }
            return View();
        }

        public JsonResult GetAllLocation()
        {
            List<formation> ls = formationService.GetMany().ToList();
            var data = ls.Select(S => new
            {
                id=S.id,
                nom_formation=S.nom_formation,
                Address = S.adresse,
                Lat = S.Lat,
                Long = S.Long,
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Map()
        {
            List<formation> ls = formationService.GetMany().ToList();
            
            ViewBag.date = ls.Select(S => new
            {
                id = S.id,
                nom_formation = S.nom_formation,
                adresse = S.adresse,
                Lat = S.Lat,
                Long = S.Long,
            });
            return View();
        }
        
        public ActionResult DeleteFormation(int id)
        {
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("http://localhost:9080");
            Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage responce = Client.DeleteAsync("pidev-web/api/projects/formation/" + id).Result;
            TempData["MessageDD"] = "Formation Deleted Successfully";
            return RedirectToAction("IndexFormation");
            
        }

        public ActionResult UpdateFormation(int id)
        {
            UserVM userConnected = Session["UserConnected"] as UserVM;
            if (userConnected == null)
            {
                return RedirectToAction("GetUser");
            }
            formation f = new formation();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:9080");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("pidev-web/api/projects/formation/"+id).Result;
            if (response.IsSuccessStatusCode)
            {
                f = response.Content.ReadAsAsync<formation>().Result;
            }
            else
            {
                ViewBag.result = "error";
            }
            return View(f);
        }
        [HttpPost]
        public ActionResult UpdateFormation(formation formation)
        {
            if (ModelState.IsValid)
            {
                HttpClient client = new HttpClient();

                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Put, "http://localhost:9080/pidev-web/api/projects/formation/update/"+formation.id);
                string json = new JavaScriptSerializer().Serialize(new
                {
                    description_formation = formation.description_formation,
                    nom_formation = formation.nom_formation,
                    prix_formation = formation.prix_formation,
                    niveau_formation = formation.niveau_formation,
                    duree_formation = formation.duree_formation
                });

                requestMessage.Content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = client.SendAsync(requestMessage).GetAwaiter().GetResult();
                TempData["Message"] = "Formation Details Edited Successfully";
                
            }
            else
            {
                return View();
            }
            return RedirectToAction("IndexFormation");
        }

        public ActionResult CreateFormation()
        {
            UserVM userConnected = Session["UserConnected"] as UserVM;
            if (userConnected == null)
            {
                return RedirectToAction("GetUser");
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateFormation(formation formation)
        {
            if (ModelState.IsValid) { 
            HttpClient client = new HttpClient();

            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, "http://localhost:9080/pidev-web/api/projects/formation");
            string json = new JavaScriptSerializer().Serialize(new
            {
                description_formation = formation.description_formation,
                nom_formation = formation.nom_formation,
                prix_formation = formation.prix_formation,
                niveau_formation = formation.niveau_formation,
                duree_formation = formation.duree_formation
            });

            requestMessage.Content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.SendAsync(requestMessage).GetAwaiter().GetResult();
                TempData["MessageC"] = "Formation Created Successfully";
                
            }
            else
            {
                return View();
            }
            return RedirectToAction("IndexFormation");
        }

        public ActionResult AffecterEtMail(int id)
        {
            List <Users> malistee = Session["SelectedUsers"] as List <Users>;
            foreach (var item in malistee)
            {
                item.FormationID = id;
                userService.Update(item);
                userService.Commit();
                formation f = formationService.GetById(id);
                MailMessage mm = new MailMessage("codinglanderspidev@gmail.com", item.Email);
                mm.Subject = "Affectation a la formation "+f.nom_formation;
                mm.Body = "Mr/Mme "+item.FullName+" Suite a votre résultat dans le quiz passé vous serez affectés ....";
                mm.IsBodyHtml = false;
                SmtpClient smpt = new SmtpClient();
                smpt.Host = "smtp.gmail.com";
                smpt.Port = 587;
              //  smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
                smpt.EnableSsl = true;
                NetworkCredential nc = new NetworkCredential("codinglanderspidev@gmail.com", "walid123&");
                smpt.UseDefaultCredentials = true;
                smpt.Credentials = nc;
                smpt.Send(mm);
                TempData["MessageKK"] = "Utilisateurs affectés et mails envoyés";
            }
            return RedirectToAction("IndexFormation");
        }

        public void ExpotToExcel()
        {
            List<Resultat> ls = resultats.GetMany().ToList();
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");
            ws.Cells["A1"].Value = "Admin";
            ws.Cells["B1"].Value = "Gary Gary";

            ws.Cells["A2"].Value = "Report";
            ws.Cells["B2"].Value = "Resultats employés";

            ws.Cells["A3"].Value = "Date";
            ws.Cells["B3"].Value = string.Format("{0:dd MMMM yyyy} at {0:H: mm tt}", DateTimeOffset.Now);

            ws.Cells["A6"].Value = "Numéro";
            ws.Cells["B6"].Value = "Employé";
            ws.Cells["C6"].Value = "Quiz passé";
            ws.Cells["D6"].Value = "Note";
            ws.Cells["E6"].Value = "Date passage quiz";
            int rowStart = 7;
            int x = 1;
            foreach (var item in ls)
            {
                if (item.NoteFloat < 50)
                {
                    ws.Row(rowStart).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Row(rowStart).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(string.Format("red")));
                }
                else if(item.NoteFloat >= 50 && item.NoteFloat < 75) {
                    ws.Row(rowStart).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Row(rowStart).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(string.Format("yellow")));
                }
                else
                {
                    ws.Row(rowStart).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Row(rowStart).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(string.Format("blue")));
                }
                ws.Cells[string.Format("A{0}", rowStart)].Value = x;
                ws.Cells[string.Format("B{0}", rowStart)].Value = item.FullName;
                ws.Cells[string.Format("C{0}", rowStart)].Value = item.NomQuiz;
                ws.Cells[string.Format("D{0}", rowStart)].Value = item.NoteFloat;
                ws.Cells[string.Format("E{0}", rowStart)].Value = string.Format("{0:dd MMMM yyyy} at {0:H: mm tt}", item.DatePassageExam);
                x++;
                rowStart++;
            }
            ws.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "ExcelReport.xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }

        public ActionResult AddVideo()
        {
            var formations = formationService.GetMany();
            ViewBag.listprod = new SelectList(formations, "id", "nom_formation");
            return View();
        }
        [HttpPost]
        public ActionResult AddVideo(Video video,HttpPostedFileBase videofile)
        {
            if (!ModelState.IsValid || videofile == null || videofile.ContentLength == 0 )
            {
                return RedirectToAction("AddVideo");
            }
            Video v = new Video
            {
                FormationID = video.FormationID,
                NomV= video.NomV,
                PathV = videofile.FileName
            };
            vss.Add(v);
            vss.Commit();
            var path = Path.Combine(Server.MapPath("~/Content/VideoFiles/"), videofile.FileName);
            videofile.SaveAs(path);
            return RedirectToAction("AddVideo");
        }

        public ActionResult ListVideo()
        {
            IEnumerable<Video> vs = vss.GetMany();
            return View(vs);
        }

        public JsonResult IndexEvent()
        {
            List<QuizVM2> events = new List<QuizVM2>();
            var qs = quizService.GetMany().ToList();
            
            foreach (var item in qs)
               
            {
                events.Add(new QuizVM2
                {
                    QuizID = item.QuizID,
                    QuizName = item.QuizName,
                    DateQuizStart=item.DateQuizStart,
                    Color=item.Color
                }); 
            }
            return Json(events.ToArray(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetEventsUser()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetEvents()
        {
            return View();
        }
   

        [HttpPost]
        public JsonResult DeleteEvent(int eventID)
        {
            
            Quiz v = quizService.GetById(eventID);
            quizService.Delete(v);
            quizService.Commit();
            var status = true;
            return new JsonResult { Data = new { status = status } };
        }

        [HttpPost]
        public JsonResult SaveEvent(QuizVM2 e)
        {
                var status = false;

                var v = quizService.GetById(e.QuizID);
                if (v != null)
                {
                    v.QuizName = e.QuizName;
                    v.DateQuizStart = e.DateQuizStart;
                    v.Color = e.Color;
                    quizService.Update(v);
                    quizService.Commit();

                }
                else
                {
                    Quiz qq = new Quiz();

                    qq.QuizName = e.QuizName;
                    qq.DateQuizStart = e.DateQuizStart;
                   
                    quizService.Add(qq);
                    quizService.Commit();
                }
             
                status = true;
                return new JsonResult { Data = new { status = status } };
        }
    }
}