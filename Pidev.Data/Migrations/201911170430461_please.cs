namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class please : DbMigration
    {
        public override void Up()
        {
           
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Resultats",
                c => new
                    {
                        ResultatID = c.Int(nullable: false, identity: true),
                        Note = c.Int(nullable: false),
                        UsersID = c.Int(),
                        QuizID = c.Int(),
                    })
                .PrimaryKey(t => t.ResultatID);
            
            CreateIndex("dbo.Resultats", "QuizID");
            CreateIndex("dbo.Resultats", "UsersID");
            AddForeignKey("dbo.Resultats", "UsersID", "dbo.Users", "UsersID");
            AddForeignKey("dbo.Resultats", "QuizID", "dbo.Quizs", "QuizID");
        }
    }
}
