namespace Pidev.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Pidev.Domain.Entities;
    using Pidev.Data.Configurations;

    public partial class MyContext : DbContext
    {
        public MyContext()
            : base("name=MyContext")
        {
           //this.Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<formation> formations { get; set; }
        public virtual DbSet<user> users { get; set; }
        public virtual DbSet<Answer> Answers { get; set; }
        public virtual DbSet<Choice> Choices { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<Quiz> Quizs { get; set; }
        public virtual DbSet<Users> Userss { get; set; }
        public virtual DbSet<Resultat> Resultats { get; set; }
        public virtual DbSet<Video> Videos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new VideoConfiguration());
            modelBuilder.Configurations.Add(new QuestionConfiguration());
            modelBuilder.Configurations.Add(new AnswerConfiguration());
            modelBuilder.Configurations.Add(new ChoiceConfiguration());
            modelBuilder.Configurations.Add(new ResultatConfiguration());
            modelBuilder.Configurations.Add(new UsersConfiguration());

            modelBuilder.Entity<formation>()
                .Property(e => e.description_formation)
                .IsUnicode(false);

            modelBuilder.Entity<formation>()
                .Property(e => e.duree_formation)
                .IsUnicode(false);

            modelBuilder.Entity<formation>()
                .Property(e => e.niveau_formation)
                .IsUnicode(false);

            modelBuilder.Entity<formation>()
                .Property(e => e.nom_formation)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.addresse)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.cin)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.login)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.nom)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.prenom)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .HasMany(e => e.formations)
                .WithMany(e => e.users)
                .Map(m => m.ToTable("formation_user").MapLeftKey("users_id").MapRightKey("Formation_id"));
        }
    }
}
