﻿using Pidev.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Data.Configurations
{
    public class QuestionConfiguration : EntityTypeConfiguration<Question>
    {
        public QuestionConfiguration()
        {
            HasOptional(p => p.Quiz)   
                .WithMany(c => c.Questions)
                .HasForeignKey(p => p.QuizID)
                .WillCascadeOnDelete(true);

        }
    }
}
