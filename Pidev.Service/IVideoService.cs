﻿using Pidev.Domain.Entities;
using Service.Pattern;

namespace Pidev.Service
{
    public interface IVideoService : IService<Video>
    {
    }
}