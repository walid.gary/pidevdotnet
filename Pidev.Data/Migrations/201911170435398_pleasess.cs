namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pleasess : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Resultats",
                c => new
                    {
                        ResultatID = c.Int(nullable: false, identity: true),
                        Note = c.Int(nullable: false),
                        UsersID = c.Int(),
                        QuizID = c.Int(),
                        DatePassageExam = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.ResultatID)
                .ForeignKey("dbo.Quizs", t => t.QuizID)
                .ForeignKey("dbo.Users", t => t.UsersID)
                .Index(t => t.UsersID)
                .Index(t => t.QuizID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Resultats", "UsersID", "dbo.Users");
            DropForeignKey("dbo.Resultats", "QuizID", "dbo.Quizs");
            DropIndex("dbo.Resultats", new[] { "QuizID" });
            DropIndex("dbo.Resultats", new[] { "UsersID" });
            DropTable("dbo.Resultats");
        }
    }
}
