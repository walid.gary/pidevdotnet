namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class req : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.formation", "description_formation", c => c.String(nullable: false, maxLength: 255, unicode: false));
            AlterColumn("dbo.formation", "duree_formation", c => c.String(nullable: false, maxLength: 255, unicode: false));
            AlterColumn("dbo.formation", "niveau_formation", c => c.String(nullable: false, maxLength: 255, unicode: false));
            AlterColumn("dbo.formation", "nom_formation", c => c.String(nullable: false, maxLength: 255, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.formation", "nom_formation", c => c.String(maxLength: 255, unicode: false));
            AlterColumn("dbo.formation", "niveau_formation", c => c.String(maxLength: 255, unicode: false));
            AlterColumn("dbo.formation", "duree_formation", c => c.String(maxLength: 255, unicode: false));
            AlterColumn("dbo.formation", "description_formation", c => c.String(maxLength: 255, unicode: false));
        }
    }
}
