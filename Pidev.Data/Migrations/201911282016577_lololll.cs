namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lololll : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Resultats", "QuizID", "Quizs");
            DropForeignKey("Resultats", "UsersID", "Users");
            AddForeignKey("Resultats", "QuizID", "Quizs", "QuizID", cascadeDelete: true);
            AddForeignKey("Resultats", "UsersID", "Users", "UsersID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("Resultats", "UsersID", "Users");
            DropForeignKey("Resultats", "QuizID", "Quizs");
            AddForeignKey("Resultats", "UsersID", "Users", "UsersID");
            AddForeignKey("Resultats", "QuizID", "Quizs", "QuizID");
        }
    }
}
