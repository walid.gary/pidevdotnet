namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class vvv : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Resultats", "FullName", c => c.String(unicode: false));
            AddColumn("dbo.Resultats", "NomQuiz", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Resultats", "NomQuiz");
            DropColumn("dbo.Resultats", "FullName");
        }
    }
}
