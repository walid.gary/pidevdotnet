namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class vvv1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Quizs", "Color", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Quizs", "Color");
        }
    }
}
