namespace Pidev.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Users
    {
        public int UsersID { get; set; }
        public string FullName { get; set; }
        public string ProfilImage { get; set; }
        public string Email { get; set; }
        public ICollection<Resultat> Resultats { get; set; }
        public int? FormationID { get; set; }
        [ForeignKey("FormationID")]
        public formation formation { get; set; }
    }
}
