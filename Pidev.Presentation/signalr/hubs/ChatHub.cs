﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Pidev.Presentation.Models;

namespace Pidev.Presentation.signalr.hubs
{
    public class ChatHub : Hub
    {
        public void Send(string name,string message)
        {
            Clients.All.addNewMessageToPage(name, message); 
        }
        public static int counter = 0;
        public override System.Threading.Tasks.Task OnConnected()
        {
            counter = counter + 1;
            Clients.All.updatecounter(counter);
            return base.OnConnected();
        }
        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            counter = counter - 1;
            Clients.All.updatecounter(counter);
            return base.OnDisconnected(stopCalled);
        }
    }
}