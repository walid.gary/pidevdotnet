﻿using Pidev.Domain.Entities;
using Service.Pattern;
using System.Collections.Generic;

namespace Pidev.Service
{
    public interface IUsersService : IService<Users>
    {
        Users GetUsersByFullName(Users user);
        Users GetUsersByFullNameee(string user);
        Users GetUsersByFor(int d);
        IEnumerable<Users> GetByIdF(int id);

    }
}