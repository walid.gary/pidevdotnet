namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ad : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.formation", "Lat", c => c.Double(nullable: false));
            AddColumn("dbo.formation", "Long", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("dbo.formation", "Long");
            DropColumn("dbo.formation", "Lat");
        }
    }
}
