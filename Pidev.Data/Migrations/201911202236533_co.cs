namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class co : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "QuestionText", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Answers", "QuestionText");
        }
    }
}
