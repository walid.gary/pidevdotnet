﻿using Pidev.Domain.Entities;
using Service.Pattern;
using Solution.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Service
{
    public class QuizService : Service<Quiz>, IQuizService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);
        public QuizService() : base(utk)
        {

        }
        public Quiz GetQuizByText (string txt)
        {
            return Get(f => f.QuizName.Equals(txt));
        }
    }
}
