﻿using Pidev.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Data.Configurations
{
    class UsersConfiguration : EntityTypeConfiguration<Users>
    {
        public UsersConfiguration()
        {
            HasOptional(p => p.formation)  
                .WithMany(c => c.Userss)
                .HasForeignKey(p => p.FormationID)
                .WillCascadeOnDelete(true);

        }
    }
}
