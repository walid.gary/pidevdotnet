namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dsqkjdgqdsiudisqkjdhqsghdqs : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Answers", "QuestionID", "Questions");
            DropForeignKey("Choices", "QuestionID", "Questions");
            DropForeignKey("Questions", "QuizID", "Quizs");
            AddForeignKey("Answers", "QuestionID", "Questions", "QuestionID");
            AddForeignKey("Choices", "QuestionID", "Questions", "QuestionID");
            AddForeignKey("Questions", "QuizID", "Quizs", "QuizID");
        }
        
        public override void Down()
        {
            DropForeignKey("Questions", "QuizID", "Quizs");
            DropForeignKey("Choices", "QuestionID", "Questions");
            DropForeignKey("Answers", "QuestionID", "Questions");
            AddForeignKey("Questions", "QuizID", "Quizs", "QuizID", cascadeDelete: true);
            AddForeignKey("Choices", "QuestionID", ".Questions", "QuestionID", cascadeDelete: true);
            AddForeignKey("Answers", "QuestionID", "Questions", "QuestionID", cascadeDelete: true);
        }
    }
}
