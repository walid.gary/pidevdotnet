namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hh : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Resultats", "Note", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Resultats", "Note", c => c.Int(nullable: false));
        }
    }
}
