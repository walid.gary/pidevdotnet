﻿using Pidev.Domain.Entities;
using Pidev.Presentation.Models;
using Pidev.Service;
using System.Web.Mvc;
namespace Pidev.Presentation.Controllers
{
    public class QQAController : Controller
    {
        IAnswerService answerService;
        IQuestionService questionService;
        IQuizService quizService;
        public QQAController()
        {
            questionService = new QuestionService();
            quizService = new QuizService();
            answerService = new AnswerService();
        }
        
        public ActionResult Index()
        {
            return View();
        }
       
        [HttpGet]
        public ActionResult AddQ()
        {
            return View();
        }

        // POST: Questions/Create
        [HttpPost]
        public ActionResult AddQ(DoItInOneStep allInOne)
        {
            Quiz Quizz = new Quiz
            {
                QuizName = allInOne.QuizName
            };
            quizService.Add(Quizz);
            quizService.Commit();
            Session["Quiz"] = Quizz;
            Question Questionn = new Question
            {
                QuizID = quizService.GetQuizByText(allInOne.QuizName).QuizID,
                QuestionText = allInOne.QuestionText,
                choix1 = allInOne.choix1,
                choix2 = allInOne.choix2,
                choix3 = allInOne.choix3,
                choix4 = allInOne.choix4
            };
            questionService.Add(Questionn);
            questionService.Commit();

            Answer qq = new Answer
            {
                AnswerText = allInOne.AnswerQ,
                QuestionID = questionService.GetQuesByText(allInOne.QuestionText).QuestionID

            };
            answerService.Add(qq);
            answerService.Commit();
            TempData["MessageQ"] = "Quiz Added Successfully";
            return RedirectToAction("AddQ");
        }
        public ActionResult AddMoreQ(DoItInOneStep allInOne)
        {
            Quiz Quizz = Session["Quiz"] as Quiz;
            Question Questionn = new Question
            {
                QuizID = Quizz.QuizID,
                QuestionText = allInOne.QuestionText,
                choix1 = allInOne.choix1,
                choix2 = allInOne.choix2,
                choix3 = allInOne.choix3,
                choix4 = allInOne.choix4
            };
            questionService.Add(Questionn);
            questionService.Commit();

            Answer qq = new Answer
            {
                AnswerText = allInOne.AnswerQ,
                QuestionID = questionService.GetQuesByText(allInOne.QuestionText).QuestionID

            };
            answerService.Add(qq);
            answerService.Commit();
            TempData["MessageQues"] = "Question Added Successfully";
            return RedirectToAction("AddQ");
            
        }
       
    }
}