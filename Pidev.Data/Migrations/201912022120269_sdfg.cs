namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sdfg : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Videos",
                c => new
                    {
                        VideoId = c.Int(nullable: false, identity: true),
                        NomV = c.String(unicode: false),
                        PathV = c.String(unicode: false),
                        FormationID = c.Int(),
                    })
                .PrimaryKey(t => t.VideoId)
                .ForeignKey("dbo.formation", t => t.FormationID, cascadeDelete: true)
                .Index(t => t.FormationID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Videos", "FormationID", "dbo.formation");
            DropIndex("dbo.Videos", new[] { "FormationID" });
            DropTable("dbo.Videos");
        }
    }
}
