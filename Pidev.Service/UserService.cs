﻿using Pidev.Data;
using Pidev.Domain.Entities;
using Service.Pattern;
using Solution.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Service
{
    public class UserService : Service<user>, IUserService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);
        public UserService() : base(utk)
        {

        }
        public IEnumerable<user> GetByEmail(string email)
        {
            return GetMany(f => f.email.Equals(email));
        }

        public user GetUserByEmail(string email)
        {
            return Get(f => f.email.Equals(email));
        }
    }
}
