﻿using Pidev.Domain.Entities;
using Service.Pattern;
using Solution.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Service
{
    public class UsersService : Service<Users>, IUsersService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);
        public UsersService() : base(utk)
        {

        }
        public Users GetUsersByFullName(Users user)
        {
            return Get(f => f.FullName.Equals(user.FullName));
        }
        public Users GetUsersByFullNameee(string user)
        {
            return Get(f => f.FullName.Equals(user));
        }
        public Users GetUsersByFor(int d)
        {
            return Get(f => f.FormationID==d);
        }
        public IEnumerable<Users> GetByIdF(int id)
        {
            return GetMany(f => f.FormationID == id);
        }
    }
}
