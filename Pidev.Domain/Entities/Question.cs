using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pidev.Domain.Entities

{

    public  class Question
    {
        public Question()
        {
            this.Answers = new HashSet<Answer>();
            this.Choices = new HashSet<Choice>();
        }
    
        public int QuestionID { get; set; }
        public string QuestionText { get; set; }
        public string choix1 { get; set; }
        public string choix2 { get; set; }
        public string choix3 { get; set; }
        public string choix4 { get; set; }
        

        public int? QuizID { get; set; }
        [ForeignKey("QuizID")]

        public virtual Quiz Quiz { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual ICollection<Choice> Choices { get; set; }
        
    }
}
