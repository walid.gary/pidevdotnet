namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class zea : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Quizs", "DateQuizStart", c => c.DateTime(precision: 0));
            AddColumn("dbo.Quizs", "DateQuizEnd", c => c.DateTime(precision: 0));
            DropColumn("dbo.Quizs", "DateQuiz");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Quizs", "DateQuiz", c => c.DateTime(precision: 0));
            DropColumn("dbo.Quizs", "DateQuizEnd");
            DropColumn("dbo.Quizs", "DateQuizStart");
        }
    }
}
