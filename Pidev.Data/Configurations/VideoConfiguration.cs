﻿using Pidev.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Data.Configurations
{
    public class VideoConfiguration : EntityTypeConfiguration<Video>
    {
        public VideoConfiguration()
        {
            HasOptional(p => p.formation)
                .WithMany(c => c.Videos)
                .HasForeignKey(p => p.FormationID)
                .WillCascadeOnDelete(true);

        }
    }
}
