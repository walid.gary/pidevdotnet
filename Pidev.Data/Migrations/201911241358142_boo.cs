namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class boo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Resultats", "isChecked", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Resultats", "isChecked");
        }
    }
}
