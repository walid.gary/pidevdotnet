﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Domain.Entities
{
    [Table("formation")]
    public class formation
    {
        public formation()
        {
            users = new HashSet<user>();
            Userss = new HashSet<Users>();

        }

        public int id { get; set; }

        [StringLength(255)]
        [Required]
        [DisplayName("Description de la formation")]
        public string description_formation { get; set; }
        [Required]
        [StringLength(255)]
        [DisplayName("Durée de la formation")]
        public string duree_formation { get; set; }
        [Required]
        [StringLength(255)]
        [DisplayName("Niveau de la formation")]
        public string niveau_formation { get; set; }
        [Required]
        [StringLength(255)]
        [DisplayName("Nom de la formation")]
        public string nom_formation { get; set; }
        [Required]
        [DisplayName("Prix de la formation")]
        public float prix_formation { get; set; }
        public string adresse { get; set; }
        public Nullable<double> Lat { get; set; }
        public Nullable<double> Long { get; set; }
        public int? file_id { get; set; }

        public virtual ICollection<user> users { get; set; }
        public virtual ICollection<Video> Videos { get; set; }

        public virtual ICollection<Users> Userss { get; set; }

    }
}
