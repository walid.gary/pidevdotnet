namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dkjfs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Quizs", "DateQuiz", c => c.DateTime(precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Quizs", "DateQuiz");
        }
    }
}
