namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ch : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "choix1", c => c.String(unicode: false));
            AddColumn("dbo.Questions", "choix2", c => c.String(unicode: false));
            AddColumn("dbo.Questions", "choix3", c => c.String(unicode: false));
            AddColumn("dbo.Questions", "choix4", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Questions", "choix4");
            DropColumn("dbo.Questions", "choix3");
            DropColumn("dbo.Questions", "choix2");
            DropColumn("dbo.Questions", "choix1");
        }
    }
}
