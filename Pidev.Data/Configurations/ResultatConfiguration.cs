﻿using Pidev.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Data.Configurations
{
    public class ResultatConfiguration : EntityTypeConfiguration<Resultat>
    {
        public ResultatConfiguration()
        {
            HasOptional(p => p.Users)   
                .WithMany(c => c.Resultats)
                .HasForeignKey(p => p.UsersID)
                .WillCascadeOnDelete(true);

            HasOptional(p => p.Quiz)   
                .WithMany(c => c.Resultats)
                .HasForeignKey(p => p.QuizID)
                .WillCascadeOnDelete(true);
        }
    }
}
