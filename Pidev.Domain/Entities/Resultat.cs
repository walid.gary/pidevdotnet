﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Domain.Entities
{
    public class Resultat { 
        public int ResultatID { get; set; }
        public string Note { get; set; }
        public string FullName { get; set; }
        public string NomQuiz { get; set; }
        public int? UsersID { get; set; }
        public Users Users { get; set; }
        public int? QuizID { get; set; }
        public Quiz Quiz { get; set; }
        public DateTime DatePassageExam { get; set; }
        public float NoteFloat { get; set; }
        public bool isChecked { get; set; }

    }
}
