namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ioi : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Resultats", "NoteFloat", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Resultats", "NoteFloat");
        }
    }
}
