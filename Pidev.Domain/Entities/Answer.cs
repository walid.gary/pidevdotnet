namespace Pidev.Domain.Entities

{
    using System;
    using System.Collections.Generic;
    
    public class Answer
    {
        public int AnswerID { get; set; }
        public string AnswerText { get; set; }
        public string QuestionText { get; set; }
        public int? QuestionID { get; set; }
        
        public virtual Question Question { get; set; }
    }
}
