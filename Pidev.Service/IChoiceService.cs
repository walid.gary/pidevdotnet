﻿using Pidev.Domain.Entities;
using Service.Pattern;
using System.Collections.Generic;

namespace Pidev.Service
{
    public interface IChoiceService : IService<Choice>
    {
        IEnumerable<Choice> GetChoiceQuestion(int id);
    }
}