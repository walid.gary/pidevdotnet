namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lolol : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Users", "FormationID", "formation");
            AddForeignKey("Users", "FormationID", "formation", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("Users", "FormationID", "formation");
            AddForeignKey("Users", "FormationID", "formation", "id");
        }
    }
}
