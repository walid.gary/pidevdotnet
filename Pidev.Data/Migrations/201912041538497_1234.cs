namespace Pidev.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1234 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Answers", "QuestionID", "Questions");
            DropForeignKey("Choices", "QuestionID", "Questions");
            DropForeignKey("Questions", "QuizID", "Quizs");
            AddForeignKey("Answers", "QuestionID", "Questions", "QuestionID", cascadeDelete: true);
            AddForeignKey("Choices", "QuestionID", "Questions", "QuestionID", cascadeDelete: true);
            AddForeignKey("Questions", "QuizID", "Quizs", "QuizID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("Questions", "QuizID", "Quizs");
            DropForeignKey("Choices", "QuestionID", "Questions");
            DropForeignKey("Answers", "QuestionID", "Questions");
            AddForeignKey("Questions", "QuizID", "Quizs", "QuizID");
            AddForeignKey("Choices", "QuestionID", "Questions", "QuestionID");
            AddForeignKey("Answers", "QuestionID", "Questions", "QuestionID");
        }
    }
}
