﻿using Pidev.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pidev.Presentation.Controllers
{
    public class ResultatController : Controller
    {
        IResultatService rs;
        public ResultatController()
        {
            rs = new ResultatService();
        }
        // GET: Resultat
        public ActionResult Index()
        {
            var result = rs.GetMany();
            return View(result.ToList());
        }
    }
}