﻿using Pidev.Domain.Entities;
using Service.Pattern;
using Solution.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidev.Service
{
    public class ChoiceService : Service<Choice>, IChoiceService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);
        public ChoiceService() : base(utk)
        {

        }
        public IEnumerable<Choice> GetChoiceQuestion (int id)
        {
            return GetMany(f => f.Question.QuestionID == id);
        }
    }
}
