﻿using Pidev.Domain.Entities;
using Service.Pattern;
using System.Collections.Generic;
using System.Linq;

namespace Pidev.Service
{
    public interface IQuestionService : IService<Question>
    {
        IQueryable<Question> GetQuestionByQuiz2(int QuizID);
        IEnumerable<Question> GetQuestionByQuiz(int QuizID);
        Question GetQuesByText(string txt);
    }
}