﻿using Pidev.Domain.Entities;
using Service.Pattern;

namespace Pidev.Service
{
    public interface IAnswerService : IService<Answer>
    {
        Answer GetAnswerByQuestion (int? q);
       
    }
}